//! Not all that happy with this code.
//! It's rather slow (much faster if you run it with `--release`)
//! but idk any other way to achive the same result.

use std::fs;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new<T: Into<i32>>(x: T, y: T) -> Point {
        Point {
            x: x.into(),
            y: y.into(),
        }
    }

    /// Return the manhatten distance from the origin (0, 0).
    fn manhatten_distance(&self) -> i32 {
        self.x + self.y
    }
}

struct Wire {
    points: Vec<Point>,
}

impl Wire {
    fn new(path: &str) -> Result<Wire, &'static str> {
        let mut x: i32 = 0;
        let mut y: i32 = 0;
        let mut points: Vec<Point> = vec![];

        for instruction in path.split(",") {
            let instruction = instruction.split_at(1);
            let distance: i32 = instruction.1.parse().unwrap();
            match instruction.0 {
                "R" => {
                    for _ in 0..distance {
                        x += 1;
                        points.push(Point::new(x, y))
                    }
                }
                "L" => {
                    for _ in 0..distance {
                        x -= 1;
                        points.push(Point::new(x, y))
                    }
                }
                "U" => {
                    for _ in 0..distance {
                        y += 1;
                        points.push(Point::new(x, y))
                    }
                }
                "D" => {
                    for _ in 0..distance {
                        y -= 1;
                        points.push(Point::new(x, y))
                    }
                }
                _ => return Err("Invalid path"),
            }
        }
        Ok(Wire { points })
    }

    fn intersects(&self, other: &Wire) -> Vec<Point> {
        let mut intersections: Vec<Point> = vec![];
        for a in &self.points {
            for b in &other.points {
                if a == b {
                    intersections.push(*a);
                }
            }
        }
        intersections
    }

    fn steps_to(&self, point: &Point) -> Result<usize, &'static str> {
        match self.points.iter().position(|p| p == point) {
            Some(p) => Ok(p + 1),
            None => Err("Point doesn't lay on wire")
        }
    }
}

fn closest(points: &Vec<Point>) -> Point {
    let mut closest = points[0];

    for point in points {
        if point.manhatten_distance() < closest.manhatten_distance() {
            closest = *point;
        }
    }
    closest
}

fn least_steps(wire: &Wire, points: &Vec<Point>) -> Point {
    let mut least_steps = points[0];

    for point in points {
        if wire.steps_to(point) < wire.steps_to(&least_steps) {
            least_steps = *point
        }
    }

    least_steps
}

fn main() {
    let input = fs::read_to_string("three/input.txt").unwrap();
    let wires: Vec<&str> = input.lines().collect();
    let wire1 = Wire::new(wires[0]).unwrap();
    let wire2 = Wire::new(wires[1]).unwrap();
    let intersections = wire1.intersects(&wire2);
    println!(
        "Closest intersection distance: {}",
        closest(&intersections).manhatten_distance()
    );

    println!(
        "Combinded least steps: {}",
        wire1.steps_to(&least_steps(&wire1, &intersections)).unwrap()
            + wire2.steps_to(&least_steps(&wire1, &intersections)).unwrap()
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_wire() {
        let points = vec![
            Point::new(8, 0),
            Point::new(8, 5),
            Point::new(3, 5),
            Point::new(3, 2),
        ];

        for point in points {
            assert_eq!(
                Wire::new("R8,U5,L5,D3").unwrap().points.contains(&point),
                true
            )
        }
    }

    #[test]
    fn test_intersects() {
        let intersections = vec![Point::new(6, 5), Point::new(3, 3)];

        let wire1 = Wire::new("R8,U5,L5,D3").unwrap();
        let wire2 = Wire::new("U7,R6,D4,L4").unwrap();

        assert_eq!(wire1.intersects(&wire2), intersections)
    }

    #[test]
    fn test_manhatten_distance() {
        assert_eq!(Point::new(3, 3).manhatten_distance(), 6)
    }

    #[test]
    fn test_closest() {
        let points = vec![Point::new(1, 2), Point::new(3, 2)];

        assert_eq!(closest(&points), Point::new(1, 2))
    }

    #[test]
    fn test_steps_to() {
        assert_eq!(
            Wire::new("R8,U5,L5,D3")
                .unwrap()
                .steps_to(&Point::new(3, 3)).unwrap(),
            20
        );
        assert_eq!(
            Wire::new("U7,R6,D4,L4")
                .unwrap()
                .steps_to(&Point::new(3, 3)).unwrap(),
            20
        );
        assert_eq!(
            Wire::new("R8,U5,L5,D3")
                .unwrap()
                .steps_to(&Point::new(0, 3)).is_err(),
            true
        );
    }
}
